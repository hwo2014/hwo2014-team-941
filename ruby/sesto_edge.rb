class SestoEdge
  attr_accessor :length

  def set_length(piece, from_lane, to_lane)
    if piece['length']
      if from_lane == to_lane
        @length = piece['length']
      else
        distance_between = (from_lane['distanceFromCenter'] - to_lane['distanceFromCenter']).abs
        @length = hypotenuse(piece['length'], distance_between)
      end
    else
      angle = piece['angle']
      radius_from = piece['radius']
      radius_to = piece['radius']
      distance_center_from = from_lane['distanceFromCenter']
      distance_center_to = to_lane['distanceFromCenter']

      if angle < 0
        radius_from += distance_center_from
        radius_to += distance_center_to
      else
        radius_from -= distance_center_from
        radius_to -= distance_center_to
      end

      if from_lane == to_lane
        @length = angle.abs*Math::PI*radius_from/180
      else
        @length = (angle.abs*Math::PI*(radius_from+radius_to)/180)/2
      end
    end
  end

  def to_s
    "#{@length}"
  end

  def weight
    return @length
  end

  private

  def hypotenuse(side1, side2)
    return Math.sqrt(side1*side1 + side2*side2)
  end
end
