require_relative 'sesto_brain'

class SestoBot

  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    @brain = SestoBrain.new
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)

    while json = tcp.gets
      message = JSON.parse(json)
      msg_type = message['msgType']
      msg_data = message['data']

      case msg_type
        when 'carPositions'
          @brain.learn_car_positions(msg_data)
          if @brain.switch_lane
            puts "switchLane #{@brain.direction}"
            tcp.puts switch_lane_message(@brain.direction)
            @brain.switch_lane = false
          elsif @brain.turbo_time?
            puts 'turbo'
            tcp.puts turbo_message('sesto goes fast!')
            @brain.turbo = false
          else
            tcp.puts throttle_message(@brain.throttle_next(message, msg_data))
          end
        else
          case msg_type
            when 'yourCar'
              puts 'yourCar'
              @brain.learn_your_car(msg_data)
            when 'gameInit'
              puts 'gameInit'
              @brain.learn_game_basics(msg_data)
            when 'join'
              puts 'join'
            when 'gameStart'
              puts 'gameStart'
              tcp.puts throttle_message(@brain.throttle)
            when 'crash'
              puts 'crash'
              @brain.learn_crash(msg_data)
            when 'spawn'
              puts 'spawn'
              @brain.learn_spawn(msg_data)
            when 'turboAvailable'
              puts 'turboAvailable'
              @brain.learn_turbo_availability(msg_data)
            when 'gameEnd'
              puts 'gameEnd'
            when 'error'
              puts "ERROR: #{msg_data}"
            else
              puts "Got unknown msgType: #{msg_type}"
              tcp.puts ping_message
          end
      end
    end
  end

  # MESSAGE BUILDING

  def turbo_message(turbo_message)
    make_msg('turbo', turbo_message)
  end

  def join_message(bot_name, bot_key)
    make_msg('join', {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg('throttle', throttle)
  end

  def ping_message
    make_msg('ping', {})
  end

  def switch_lane_message(direction)
    make_msg('switchLane', direction)
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end
