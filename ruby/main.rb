require 'json'
require 'socket'
require_relative 'sesto_bot'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

SestoBot.new(server_host, server_port, bot_name, bot_key)
