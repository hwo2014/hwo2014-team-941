require_relative 'sesto_edge'
require_relative 'sesto_graph'
require_relative 'track_piece'

class SestoBrain

=begin
  INSTANCE VARIABLES:
  @lanes
    lane data as an array, items like:
      {
        "distanceFromCenter": -20,
        "index": 0
      }
  @pieces
    piece data as an array, items like:
      {
        "length": 100.0,
        "switch": true
      },
      {
        "radius": 200,
        "angle": 22.5
      }
  @lane_count
    the number of lanes
  @piece_count
    the number of pieces
  @track_graph
    route as a graph + shortest route calculations
  @positions_msg_before
    last tick message (carPositions)
  @my_car
    my car color
  @my_car_index
    my cars index for carPositions
  @current_piece
    current track piece
  @switch_lane
    boolean - if we need to switch lane
  @direction
    what direction to switch
  @throttle
    the amount of throttle we need to use currently
  @optimum_lane
    the best lane to have on the starting piece (shortest route possible when starting here)
  @drag_coef
    calculated drag coefficient
  @mass
    calculated car mass
=end

  attr_accessor :switch_lane, :direction, :throttle, :turbo

  def initialize
    @switch_lane = false
    @direction = "Right"
    @throttle = 0.60
    @turbo = false
    @drag_coef = 0
    @mass = -1
    @predicted_velocity_x = 0
    @old_velocity_y = 0
    @predicted_velocity_y = 0
    @other_coef = 0
  end

=begin
{
  "name": "Schumacher",
  "color": "red"
}
=end
  def learn_your_car(msg_data)
    @my_car = msg_data['color']
  end

=begin
{
  "race": {
    "track": {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ]
    },
    "cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ],
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
  }
}
=end
  def learn_game_basics(msg_data)
    @lanes = msg_data['race']['track']['lanes']
    @pieces = msg_data['race']['track']['pieces']

    @long_straights = learn_long_straights

    @lane_count = @lanes.size
    @piece_count = @pieces.size

    setup_graph_and_pieces

    learn_my_car_index(msg_data)
    learn_optimum_start_lane
  end

=begin
[
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
]
=end
  def learn_car_positions(msg_data)
    if piece_crossed?(@positions_msg_before, msg_data)
      @current_piece = @current_piece.next_piece
      #puts @pieces[@current_piece.index]
      check_lane_switch(piece_index_from_data(msg_data), start_lane_index_from_data(msg_data))
    end

    @current_car_angle = car_angle(msg_data)

    @current_velocity_x = current_velocity_x(msg_data, @positions_msg_before)
    @current_velocity_y = current_velocity_y(msg_data, @positions_msg_before)

    #puts "P: #{@predicted_velocity_y} C: #{@current_velocity_y} D: #{@current_velocity_y - @predicted_velocity_y}"

    @predicted_velocity_x = future_velocity_x(@current_velocity_x, @throttle)
    @predicted_velocity_y = future_velocity_y(@current_velocity_y, @current_car_angle, @current_velocity_x, @current_piece.index)

    learn_mass_and_drag if (not @crashed) && @current_velocity_x != 0 # älä vaihda throttle ennen tätä

    @positions_msg_before = msg_data
    @old_velocity_x = @current_velocity_x
    @old_velocity_y = @current_velocity_y
  end

  def future_velocity_y(current_car_angle_speed, current_car_angle, current_speed, current_piece_index)
=begin
    if @pieces[current_piece_index]['radius']
      piece_radius = @pieces[current_piece_index]['radius']
      piece_angle = @pieces[current_piece_index]['angle']

      c_force = @mass * current_speed * current_speed / piece_radius

    end
=end
    returning_force = (current_speed * @drag_coef) * Math::sin(rad(current_car_angle))

    current_car_angle_speed + (returning_force) / @mass
  end

  def throttle_next(message, msg_data) #nuke this
    if current_tick(message) > 30
      #speed_next = speed_in_next_turn(msg_data)
      if speed_in_next_turn(msg_data, 0.2) < 6
        if speed_in_next_turn_next_tick(msg_data, 0.2) > 6
          0.2
        else
          1
        end
      else
        0.2
      end
    else
      1
    end
  end

  def future_velocity_x(current_speed, throttle)
    current_speed + (throttle - current_speed * @drag_coef) / @mass
  end

  def rad(degrees)
    degrees * Math::PI / 180
  end

=begin
{
  "name": "Rosberg",
  "color": "blue"
}
=end
  def learn_crash(msg_data)
    if msg_data['color'] == @my_car
      puts 'I crashed' #might want to do something about this
      @crashed = true
    else
      puts "#{msg_data['color']} spawned"
    end
  end

=begin
{
  "name": "Rosberg",
  "color": "blue"
}
=end
  def learn_spawn(msg_data)
    if msg_data['color'] == @my_car
      puts 'I spawned' #might want to do something about this
      @crashed = false
    else
      puts "#{msg_data['color']} spawned"
    end
  end

=begin
{
  "turboDurationMilliseconds": 500.0,
  "turboDurationTicks": 30,
  "turboFactor": 3.0
}
=end
  def learn_turbo_availability(msg_data)
    @turbo = true
  end

  def turbo_time?
    if @long_straights.include?(@current_piece.index) && @turbo
      true
    else
      false
    end
  end

  #returns list of IDs for pieces that start long straights
  def learn_long_straights
    straight = 0;
    straight_start = nil
    long_straight_starts = []
    @pieces.each_with_index do |piece, index|
      if piece['angle'].nil?
        straight_start ||= index
        straight += piece['length'] if piece['angle'].nil?
        if straight > 300 #le magic numbar
          long_straight_starts.push straight_start
        end
      else
        straight_start = nil
        straight = 0
      end
    end
    long_straight_starts.uniq
  end

  def length_to_next_turn(msg_data)
    length = 0
    piece = @current_piece
    while @pieces[piece.index]['angle'].nil?
      lane_index = start_lane_index_from_data(msg_data)
      length += piece.get_edge(lane_index, lane_index).length
      piece = piece.next_piece
    end
    length
  end

  def speed_in_next_turn(msg_data, speed = @throttle)
    length = length_to_next_turn(msg_data)
    length2 = 0
    s = @current_velocity_x
    while length2 < length
      s = future_velocity_x(s, speed)
      length2 += s
    end
    s
  end

  def speed_in_next_turn_next_tick(msg_data, speed = @throttle)
    length = length_to_next_turn(msg_data)
    length - @current_velocity_x
    length2 = 0
    s = @current_velocity_x
    while length2 < length
      s = future_velocity_x(s, speed)
      length2 += s
    end
    s
  end

  def next_turn(msg_data)
    piece = @current_piece.next_piece
    while @pieces[piece.index]['angle'].nil?
      lane_index = start_lane_index_from_data(msg_data)
      length += piece.get_edge(lane_index, lane_index).length
      piece = piece.next_piece
    end
    piece
  end

  def current_tick(message)
    if message['gameTick']
      message['gameTick']
    else
      0
    end
  end

  private
  ########################### PRIVATE METHODS ###########################

  def learn_mass_and_drag
    if @mass != -1
      guessed_speed = future_velocity_x(@old_velocity_x, @throttle)
      diff = guessed_speed - @current_velocity_x
      f_drag = @mass * diff
      @drag_coef += f_drag / @current_velocity_x
    else
      # predict mass from first movement
      @mass = @throttle / @current_velocity_x
    end
  end

  def setup_graph_and_pieces
    @track_graph = SestoGraph.new(@piece_count, @lane_count)
    last_piece = nil

    @pieces.each_with_index do |piece, piece_index|
      track_piece = TrackPiece.new(piece_index, @lane_count)
      if piece_index == 0
        @current_piece = track_piece
        last_piece = track_piece
      elsif piece_index == @piece_count - 1
        track_piece.previous_piece = last_piece
        last_piece.next_piece = track_piece
        track_piece.next_piece = @current_piece
        @current_piece.previous_piece = track_piece
      else
        track_piece.previous_piece = last_piece
        last_piece.next_piece = track_piece
        last_piece = track_piece
      end

      setup_lanes(piece, piece_index, track_piece)
    end
  end

  def setup_lanes(piece, piece_index, track_piece)
    @lanes.each_with_index do |lane, lane_index|
      # straight connection
      edge = create_edge(piece, lane, lane)
      @track_graph.set_edge(piece_index, lane_index, piece_index + 1, lane_index, edge)
      track_piece.set_edge(lane_index, lane_index, edge)

      # side connection on switch pieces
      if piece['switch']
        # switch to left lane
        if lane_index > 0
          to_lane = @lanes[lane_index - 1]
          edge = create_edge(piece, lane, to_lane)
          @track_graph.set_edge(piece_index, lane_index, piece_index + 1, lane_index - 1, edge)
          track_piece.set_edge(lane_index, lane_index - 1, edge)
        end
        # switch to right lane
        if lane_index < @lane_count - 1
          to_lane = @lanes[lane_index + 1]
          edge = create_edge(piece, lane, to_lane)
          @track_graph.set_edge(piece_index, lane_index, piece_index + 1, lane_index + 1, edge)
          track_piece.set_edge(lane_index, lane_index + 1, edge)
        end
      end
    end
  end

  def create_edge(piece, from_lane, to_lane)
    # TODO move length calc from edge to here
    edge = SestoEdge.new
    edge.set_length(piece, from_lane, to_lane)
    edge
  end

  def learn_my_car_index(msg_data)
    msg_data['race']['cars'].each_with_index do |car, index|
      if car['id']['color'] == @my_car
        @my_car_index =  index
        return
      end
    end
  end

  def path_to_optimum_start_lane(from_piece, from_lane)
    @track_graph.shortest_path(from_piece, from_lane, 0, @optimum_lane)
  end

  def direction_on_next_piece(piece_index, lane_index)
    path = path_to_optimum_start_lane(piece_index, lane_index)
    if path['pieces'].size <= 1
      return nil
    end

    m_index_current = path['pieces'][0]
    m_index_next = path['pieces'][1]
    if m_index_next != (m_index_current + @lane_count) % (@lane_count * @piece_count)
      difference = (m_index_next % @lane_count) - (m_index_current % @lane_count)
      if difference < 0
        "Left"
      else
        "Right"
      end
    else
      nil
    end
  end

  def check_lane_switch(piece_index, lane_index)
    direction = direction_on_next_piece(piece_index, lane_index)
    if direction
      @switch_lane = true
      @direction = direction
    end
  end

  def current_velocity_x(data, old_data)
    if old_data
      previous_piece_index = piece_index_from_data(old_data)
      current_piece_index = piece_index_from_data(data)
      if previous_piece_index == current_piece_index # if still in same piece
        distance_from_data(data) - distance_from_data(old_data)
      else # we have moved to next piece
        # get the length of last edge traveled
        from_lane_index = start_lane_index_from_data(old_data)
        to_lane_index = end_lane_index_from_data(old_data)
        last_edge_length = @track_graph.get_edge(previous_piece_index, from_lane_index, previous_piece_index + 1, to_lane_index).length
        # the rest of last piece + the distance traveled this piece
        (last_edge_length - distance_from_data(old_data)) + distance_from_data(data)
      end
    else
      0 #we haven't started yet
    end
  end

  def current_velocity_y(data, old_data)
    if old_data
      angle = car_angle(data) - car_angle(old_data)
      30 * rad(angle) # change to car size
    else
      0 #we haven't started yet
    end
  end

  def car_angle(data)
    data[@my_car_index]['angle']
  end

  def piece_index_from_data(data)
    data[@my_car_index]['piecePosition']['pieceIndex']
  end

  def start_lane_index_from_data(data)
    data[@my_car_index]['piecePosition']['lane']['startLaneIndex']
  end

  def end_lane_index_from_data(data)
    data[@my_car_index]['piecePosition']['lane']['endLaneIndex']
  end

  def distance_from_data(data)
    data[@my_car_index]['piecePosition']['inPieceDistance']
  end

  def piece_crossed?(old, new)
    if old
      piece_index_from_data(old) != piece_index_from_data(new)
    else
      false
    end
  end

  def learn_optimum_start_lane
    routes = []
    @lanes.each_with_index do |lane, index|
      routes.push @track_graph.shortest_path(0, index, -1, index);
    end
    routes.sort! do |route1, route2|
      route1['length'] <=> route2['length']
    end

    @optimum_lane = routes[0]['start_lane']
  end
end
