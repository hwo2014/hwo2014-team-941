class TrackPiece
  attr_accessor :next_piece, :previous_piece, :index

  def initialize(index, lane_count)
    @index = index
    @edges = []
    lane_count.times do
      @edges.push []
    end
  end

  def set_edge(from_lane, to_lane, edge)
    @edges[from_lane][to_lane] = edge
  end

  def get_edge(from_lane, to_lane)
    @edges[from_lane][to_lane]
  end
end