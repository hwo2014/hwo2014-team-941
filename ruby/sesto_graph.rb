class SestoGraph

  attr_accessor :graph_matrix

  def initialize(piece_count, lane_count)
    @graph_matrix = []
    @piece_count = piece_count
    @lane_count = lane_count

    node_count = piece_count * lane_count
    node_count.times do
      row = []
      @graph_matrix.push row
      node_count.times do
        row.push nil
      end
    end
  end

  def get_edge(from_piece, from_lane, to_piece, to_lane)
    row_index = matrix_index(from_piece, from_lane)
    col_index = matrix_index(to_piece, to_lane)
    @graph_matrix[row_index][col_index]
  end

  def set_edge(from_piece, from_lane, to_piece, to_lane, edge)
    row_index = matrix_index(from_piece, from_lane)
    col_index = matrix_index(to_piece, to_lane)
    @graph_matrix[row_index][col_index] = edge
  end

  def shortest_path(from_piece, from_lane, to_piece, to_lane)
    from_index = matrix_index(from_piece, from_lane)
    to_index = matrix_index(to_piece, to_lane)

    distances = []
    visited = []
    previous = {}

    @piece_count.times do
      @lane_count.times do
        distances.push Float::INFINITY
        visited.push false
      end
    end

    distances[from_index] = 0

    distances.each do
      current_node_index = minimum_distance_node_index(distances, visited)

      if current_node_index == to_index
        break
      end

      visited[current_node_index] = true

      distances.each_with_index do |distance, next_node_index|
        if (not visited[next_node_index]) && @graph_matrix[current_node_index][next_node_index] &&
            distances[current_node_index] != Float::INFINITY &&
            distances[current_node_index] + @graph_matrix[current_node_index][next_node_index].length < distances[next_node_index]
          distances[next_node_index] = distances[current_node_index] + @graph_matrix[current_node_index][next_node_index].length
          previous[next_node_index] = current_node_index
        end
      end
    end

    route = {}
    route['start_lane'] = from_lane
    route['length'] = distances[to_index]
    pieces = []
    route['pieces'] = pieces

    piece_index = to_index
    while previous[piece_index]
      pieces.unshift piece_index
      piece_index = previous[piece_index]
    end

    route
  end

  private

  def matrix_index(piece_index, lane_index)
    (piece_index % @piece_count) * @lane_count + lane_index
  end

  def minimum_distance_node_index(distances, visited)
    min = Float::INFINITY
    min_index = -1

    distances.each_with_index do |distance, index|
      if (not visited[index]) && distance < min
        min = distance
        min_index = index
      end
    end

    min_index
  end
end